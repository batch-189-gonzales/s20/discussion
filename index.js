/*
	While Loop

	Syntax:
	 while (condition) {
		statement
	 }
*/

let count = 5;

while (count !== 0) {
	console.log(`While ${count}`);
	count--;
};

let x = 0;

while (x < 5){
	x++;
	console.log(x);
};

/*
	Do While Loop - works a lot like the while loop. But unlike while looops, so while loops guarantee that the code will be executed at least once.

	Syntax:
	 do {
		statement
	 } while (condition)
*/

let number = Number(prompt("Give me a number: "));

do {
	console.log(`Do while ${number}`);
	number++;
} while (number < 10);

/*
	For Loop - more flexible than while loop and do while loop

	Syntax:
	 for(initialization; condition; finalExpression){
		statement
	 }
*/

for(let count = 0; count <= 20; count++){
	console.log(`For loop ${count}`);
};

let myString = "alex";
console.log(myString.length);

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
};

for(let i = 0; i < myString.length; i++){
	if(
		myString[i].toLowerCase() == "a" ||
		myString[i].toLowerCase() == "e" ||
		myString[i].toLowerCase() == "i" ||
		myString[i].toLowerCase() == "o" ||
		myString[i].toLowerCase() == "u"
	) {
		console.log("vowel");
	} else {
		console.log(myString[i]);
	};
};

/*
	Continue and Break Statements
		Continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

		Break statement is used to terminate the current loop once a match has been found
*/

for (let count = 0; count <= 20; count++) {
	if (count % 2 === 0) {
		continue;
	};

	console.log(`Continue and Break ${count}`);

	if (count > 10 ) {
		break;
	};
};

let name = "Alexandro";

for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if (name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration");
		continue;
	};

	if (name[i].toLowerCase() === "d") {
		break;
	};
};